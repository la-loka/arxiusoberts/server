# -*- coding: utf-8 -*-
from flask import abort
import os, shutil
from server import app
from datahelper import getStores


def joinPath(parentDirectory, subDirectory):
    directoryPath = os.path.join(parentDirectory, subDirectory)
    relative = os.path.relpath(directoryPath, parentDirectory)

    if relative.startswith(os.pardir):
        # think: '../../../etc/passwd'
        abort(404)
    if doesPathContainHiddenDirectories(directoryPath):
        # think: 'folder/.config/data'
        abort(404)
    if not os.path.isdir(directoryPath):
        abort(404)
    return directoryPath


'''
We don't show hidden directories
'''
def doesPathContainHiddenDirectories(path):
    dirs = os.path.abspath(path).split('/')
    for directory in dirs:
        if directory.startswith("."):
            return True
    return False


'''
Send an email to customers with webserver stats attached.
'''
def generateReports():
    import subprocess
    from datetime import datetime
    import smtplib
    from email.MIMEMultipart import MIMEMultipart
    from email.mime.application import MIMEApplication
    from email.MIMEText import MIMEText
    from email import Encoders

    if not os.path.isfile(app.config['GOACCESS_BINARY']):
        print 'Reports failed. GoAccess not found at %s' % app.config['GOACCESS_BINARY']
        return
    today = datetime.now()
    stores = getStores()
    for store in stores:
        if not store.storeID in app.config['CUSTOMERS'] or not 'WEBSERVER_LOG_FILE' in app.config['CUSTOMERS'][store.storeID]:
            print '%s config not found in customer.db, skipping.' % store.storeID
            continue
        reportFileName = "%s-%s.html" % (today.strftime('%d-%m-%Y'), store.storeID)

        bashCommand = "zcat -f %s/%s* | %s -p %s -a -o %s/%s" % (
                                app.config['WEBSERVER_LOG_PATH'],
                                app.config['CUSTOMERS'][store.storeID]['WEBSERVER_LOG_FILE'],
                                app.config['GOACCESS_BINARY'],
                                app.config['GOACCESS_CONFIG'],
                                app.config['TMP_REPORT_PATH'], reportFileName
                                )
        try:
            process = subprocess.Popen(bashCommand, stdout=subprocess.PIPE, shell=True)
            output, error = process.communicate()
            if error:
                print error
                continue
        except:
            print 'Failed to generate report for %s\n%s' % (store.storeID, bashCommand)
            continue                
        
        # Send email
        SUBJECT = "Monthly web statistics"

        msg = MIMEMultipart('text')
        msg['Subject'] = SUBJECT
        msg['From'] = app.config['EMAIL_FROM']
        msg['To'] = app.config['CUSTOMERS'][store.storeID]['CONTACT_EMAIL']
        body = 'Hola %s,\n\nAquí les estadístiques mensuals del portal de transparència.\n\nCordials salutacions.\n%s' % (app.config['CUSTOMERS'][store.storeID]['CONTACT_NAME'], app.config['EMAIL_FROM'])
        body = MIMEText(body, 'plain', 'utf-8')
        msg.attach(body)

        report = MIMEApplication('application', "octet-stream")
        report.set_payload(open("%s/%s" % (app.config['TMP_REPORT_PATH'], reportFileName), "rb").read())
        Encoders.encode_base64(report)
        report.add_header('Content-Disposition', 'attachment; filename="%s"' % reportFileName)
        msg.attach(report)

        server = smtplib.SMTP(app.config['EMAIL_SERVER'], app.config['EMAIL_PORT'])
        to_addrs = [app.config['CUSTOMERS'][store.storeID]['CONTACT_EMAIL'], app.config['EMAIL_BCC']]
        try:
            server.sendmail(app.config['EMAIL_FROM'], to_addrs, msg.as_string())
        except:
            print 'Failed to email report to %s' % app.config['CUSTOMERS'][store.storeID]['CONTACT_EMAIL']
        
        os.remove('%s/%s' % (app.config['TMP_REPORT_PATH'], reportFileName))



def checkRecollDirectories():
    stores = getStores()

    # Remove orphaned recoll dirs from deleted Stores
    (baseDirectory, recollStoreDirs, files) = os.walk(app.config['RECOLL_PATH'], topdown=True).next()
    for recollDir in recollStoreDirs:
        if not recollDir in stores: 
            shutil.rmtree(os.path.join(baseDirectory, recollDir))

    # Insure all Store.Folders have a corresponding recoll dir. 
    folderCnt = 0
    for store in stores:
        folderCnt += store.checkRecollDirectorySanity()
    print "Serving %s stores containing %s folders" % (stores.__len__(), folderCnt)


'''
Change the recoll.conf content in all Recoll directories
'''
def updateRecollConfig():
    stores = getStores()
    for store in stores:
        for folder in store.folders:
            folder.writeRecollConfig()    



