# -*- coding: utf-8 -*-
from server import app
import os, binascii, shutil
from datetime import datetime
from flask import abort
import hashlib


'''
A Folder is a directory found directly under a Store directory.
'''
class Folder():
    name = None
    store = None

    def __init__(self, store, folderName):
        self.name = folderName
        self.store = store

    def __unicode__(self):
        return self.name

    def getFilesystemPath(self):
        return os.path.join(self.store.getFilesystemPath(), self.name)

    def hashName(self):
        return hashlib.md5(self.name.encode('utf-8')).hexdigest()

    def getRecollDir(self):
        return os.path.join(self.store.getRecollDir(), self.hashName())

    def getStorageURL(self):
        try:
            if app.config['CUSTOMERS'][self.store.storeID]['DONT_INCLUDE_FOLDERNAME_IN_STORAGE_URL'] == 1:
                return self.store.getStorageURL()
        except:
            pass
        return '%s/%s' % (self.store.getStorageURL(), self.name)

    def writeRecollConfig(self):
        recollDir=self.getRecollDir()
        if not os.path.isdir(recollDir):
            os.makedirs(recollDir)
        f = open(os.path.join(recollDir, 'recoll.conf'), 'w')
        text = 'topdirs = "%s"\nskippedNames =' % self.getFilesystemPath()
        for pattern in app.config['SKIPPED_FILES']:
            text += ' %s' % pattern
        text += '\n'
        f.write(text.encode('utf-8')) 
        f.close()


'''
A Store is a directory found directly under the config['STORAGE_PATH'] directory.
'''
class Store():
    storeID = ''
    storageURL = ''
    folders = {}
    
    def __init__(self, storeID):
        (baseDirectory, dirs, files) = os.walk(app.config['STORAGE_PATH'], topdown=True).next()
        if not storeID in dirs:
            abort(404)
        self.storeID = storeID
        try:
            self.storageURL = app.config['CUSTOMERS'][self.storeID]['STORAGE_URL']
        except:
            self.storageURL = "%s/%s" % (app.config['CUSTOMERS']['DEFAULT']['STORAGE_URL'], self.storeID)
        
        (baseDirectory, dirs, files) = os.walk(self.getFilesystemPath(), topdown=True).next()
        self.folders = {}
        for folderName in dirs:
            self.folders[folderName] = Folder(self, folderName)
                
    def __unicode__(self):
        return self.storeID

    def getFilesystemPath(self):
        return os.path.join(app.config['STORAGE_PATH'], self.storeID)

    def getRecollDir(self):
        return os.path.join(app.config['RECOLL_PATH'], self.storeID)
 
    def getStorageURL(self):
        return self.storageURL
  
    def buildRecollDirectories(self):
        if not os.path.isdir(self.getFilesystemPath()):
            return

        '''
        Check that all Folders have an associated recoll directory
        Create or delete recoll directories accordingly
        '''

        recollPath=self.getRecollDir()
        if not os.path.isdir(recollPath):
            os.makedirs(recollPath)
        
        hashedFolderNames = []
        for folderName in self.folders:
            hashedFolderNames.append(self.folders[folderName].hashName())
            
        (path, recollDirs, files) = os.walk(recollPath, topdown=True).next()
        for recollDirectoryName in recollDirs:
            if not recollDirectoryName in hashedFolderNames:
                shutil.rmtree(os.path.join(path, recollDirectoryName))

        for folderName in self.folders:
            folder = self.folders[folderName]
            if not os.path.isdir(folder.getRecollDir()):
                folder.writeRecollConfig()

        return self.folders.__len__()
