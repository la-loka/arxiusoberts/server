# -*- coding: utf-8 -*-
from flask import Flask

app = Flask(__name__)
app.config.from_pyfile('config.cfg')
app.config.from_pyfile('tenant.db')

from server import views

if __name__ == '__main__':
    app.run()

