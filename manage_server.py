from flask_script import Manager, Server
from server import app
from server import utils

manager = Manager(app)

# Turn on debugger by default and reloader
manager.add_command("run", Server(
    use_debugger = True,
    use_reloader = True,
    threaded = True,
    port = '5001',
    host = '0.0.0.0')
)

'''
Run this after creating the customer directory (aka store) and subdirectories (aka store.folders)
Remember to chown -R the new recoll directory.
#usage: python manage_storage.py buildRecollDirectories id (id = the name of the customer, aka store)
'''
@manager.command
def buildRecollDirectories(id):
    from server import models
    s=models.Store(id)
    s.buildRecollDirectories()
    print "OK"


@manager.command
def generateReports():
    from server import utils
    utils.generateReports()


if __name__ == "__main__":
    manager.run()
