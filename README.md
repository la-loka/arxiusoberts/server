# Install Storage


Python 2.7 required.

## Prepare

```
apt-get install python2.7
apt-get install gcc python-dev git
apt-get install python-virtualenv
```

```
mkdir /opt/ArxiusOberts
cd /opt/ArxiusOberts
git clone https://gitlab.com/la-loka/arxiusoberts/server.git
```

## Prepare the helper script

The helper script can be used to run the a server in dev mode, and housekeeping.

```
cd /opt/ArxiusOberts/server
virtualenv -p /usr/bin/python2.7 ./venv
cp /opt/ArxiusOberts/arxiusoberts/run.py /opt/ArxiusOberts/
```

## Install python libraries

```
source venv/bin/activate
pip install flask
pip install flask-wtf
pip install flask-script
pip install flask-login
pip install markdown
```

## Configure

```
cp arxiusoberts/example.config.cfg arxiusoberts/config.cfg
cp arxiusoberts/example.tenant.cfg arxiusoberts/tenant.cfg

mkdir -p /var/www/tenant-files
mkdir /var/recoll

```
Edit `/opt/ArxiusOberts/server/config.cfg` to change these paths.


## Test run

```
cp /opt/ArxiusOberts/server/manage_server.py `/opt/ArxiusOberts/run.py
```

```
python run.py runserver

```


# Production

## Install gunicorn

```
source venv/bin/activate
pip install gunicorn (http://stackoverflow.com/questions/25611140/syntax-error-installing-gunicorn)

```

### gunicorn_config.py
Edit `/opt/ArxiusOberts/gunicorn_config.py`

```
command = '/opt/ArxiusOberts/server/venv/bin/gunicorn'
pythonpath = '/opt/ArxiusOberts'
bind = '127.0.0.1:5001'
workers = 3
user = 'www-data'
```

### Test gunicorn

#### Python2.7 egg issue

We compiled Recoll and then used an egg to install a python2.7 in the venv. This created an egg cache in /root/.cache This cache containes files that need to be opened by the python2.7 recoll installation. However, www-data user does not have access to /root, we need to copy the cache and set env.

```
mkdir /opt/ArxiusOberts/.cache
cp /root/.cache/Python_Eggs /opt/ArxiusOberts/.cache
chown -R www-data.www-data /opt/ArxiusOberts/.cache
```
Export the new path
```
export PYTHON_EGG_CACHE=/opt/ArxiusOberts/.cache/Python-Eggs/
```

Recoll will create a .recoll directory in www-data's home directory. www-data's home directory is /var/www
```
chown www-data.www-data /var/www
```
 


```
gunicorn -c /opt/ArxiusOberts/gunicorn_config.py server:app
```

# Install supervisor
Supervisor keeps gunicorn running.

```
apt-get install supervisor
vim /etc/supervisor/conf.d/almacen.conf
```

### almacen.conf
Edit `arxius.conf`

```
[program:arxius]
environment=PYTHON_EGG_CACHE=/opt/ArxiusOberts/.cache/Python-Eggs/
command = /opt/ArxiusOberts/server/venv/bin/gunicorn -c /opt/ArxiusOberts/gunicorn_config.py server:app
directory = /opt/ArxiusOberts
user = www-data
```

## Install ET-components
```
mkdir /var/www/components/
cd /var/www/components/
git clone https://gitlab.com/la-loka/arxiusoberts/et-grid.git ET-grid

```

## Nginx config
```
server {
        listen 80;
        server_name a_site.org;

        root    /var/www/portales/a_site;

        location / {
                alias /var/www/client-files/a_site/;
                try_files $uri @web;
        }
        location /w/ {
                alias /var/www/portales/a_site/;
        }
        location @web {
                root /var/www/portales/a_site/;
        }
        location /components/ {
                alias /var/www/components/;
        }
        location /api/ {
                proxy_pass http://127.0.0.1:5001/store/;
                proxy_set_header X-Forwarded-Host $server_name;
                proxy_set_header X-Real-IP $remote_addr;
                #add_header P3P 'CP="ALL DSP COR PSAa PSDa OUR NOR ONL UNI COM NAV"';
        }

        access_log /var/log/nginx/storage.access.log;
        error_log /var/log/nginx/storage.error.log notice;
}
```

# Add a new tenant

## configure storage
edit `server/tentant.db`
```
    'a_tenant': {
                'STORAGE_URL': 'https://a_site.org',
                'WEBSERVER_LOG_FILE': 'a_site.access.log',
                'CONTACT_NAME': 'Some name',
                'CONTACT_EMAIL': 'persona@example.com',
                },

```

## Add files
```
mkdir /var/www/client-files/<tenant_name>
```
Now we initiate recoll config for these new files.
```
cd /opt/arxiusoberts
source venv/bin/activate
python run.py buildRecollDirectories <tentant_name>
cd /var/www/client-files/a_tenant
chown -R syncthing.syncthing a_tentant
```

Reindex the files every night

`crontab -u syncthing -e`

```
0 1 * * * /bin/bash /opt/ArxiusOberts/server/updateRecollIndexes.sh
```
