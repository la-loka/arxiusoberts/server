# -*- coding: utf-8 -*-
from flask import request, Response, abort
from server import app
from .models import Store, Folder
import datahelper, utils
import os, binascii, json
from urllib import unquote


@app.route('/', methods=['GET'])
@app.route('/index', methods=['GET'])
def index():
    return JsonResponse("1")


@app.route('/stores', methods=['GET'])
def stores():
    data=datahelper.getStoreIDs()

    json_response = json.dumps(data)
    callback = request.args.get('callback', False)
    if callback:
        json_response = "%s(%s)" % (callback, json_response)
    return JsonResponse(json_response)


@app.route('/store/<string:storeID>', methods=['GET'])
def store(storeID):
    store = Store(storeID)
    data={'store_id':store.storeID, 'storage_url':store.storageURL}
    
    folders=[]
    for folderName in store.folders:
        folders.append(folderName)
    data['folders']=folders

    json_response = json.dumps(data)
    callback = request.args.get('callback', False)
    if callback:
        json_response = "%s(%s)" % (callback, json_response)
    return JsonResponse(json_response)


@app.route('/resource/<string:storeID>/<string:folderName>', methods=['GET'])
@app.route('/store/<string:storeID>/<string:folderName>', methods=['GET'])
def folder(storeID, folderName):
    store = Store(storeID)
    try:
        folder=store.folders[folderName]
    except:
        abort(404)
        
    if not request.args:
        return JsonResponse()
        
    callback = request.args.get('callback', False)
    
    data = None
    do = request.args.get('do')
    if do=='list':
        queriedPath = request.args.get('file')
        if not queriedPath:
            queriedPath = ""
        queriedPath = unquote(queriedPath.encode('utf-8')).decode('utf-8')
        folderPath = folder.getFilesystemPath()
        ''' 
        Make sure we're dealing with a sane path
        '''
        directoryPath = utils.joinPath(folderPath, queriedPath)  
        
        (results, nextPage)=datahelper.getDirectoryContent(folder.getStorageURL(), folderPath, queriedPath)
        description=datahelper.getDirectoryDescription(folderPath, directoryPath)
        data={'results':results, 'folder_description':description, 'nextPage':nextPage}
            
    elif do=='search':
        folderPath=folder.getFilesystemPath()
        search_text=request.args.get('search_text')
        if search_text:
            try:
                currentPage=int(request.args.get('currentPage'))
            except:
                currentPage=0
            try:
                (results, nextPage)=datahelper.search(store, folder, search_text, currentPage)
            except:
                results=[]
                nextPage=None
        else:
            (results, nextPage)=datahelper.getDirectoryContent(folder.getStorageURL(), folderPath, '')
        description=datahelper.getDirectoryDescription(folderPath)
        data={'results':results, 'folder_description':description, 'nextPage':nextPage}

    if data:      
        json_response = json.dumps(data)
        if callback:
            json_response = "%s(%s)" % (callback, json_response)
        return JsonResponse(json_response)

    abort(404)


def JsonResponse(json_response="1", status_code=200):
    response = Response(json_response, 'application/json; charset=utf-8')
    response.headers.add('content-length', len(json_response))
    response.status_code=status_code
    return response
