# -*- coding: utf-8 -*-
from server import app
import markdown, codecs#, bleach
from recoll import recoll, rclextract
import os, time
from .models import Store, Folder


def getStoreIDs():
    (baseDirectory, dirs, files) = os.walk(app.config['STORAGE_PATH'], topdown=True).next()
    return dirs

def getStores():
    stores = []
    for storeID in getStoreIDs():
        stores.append(Store(storeID))
    return stores

def getDirectoryContent(storeURL, storePath, queriedPath):
    directory = os.path.join(storePath, queriedPath)
    (baseDirectory, dirs, files) = os.walk(directory, topdown=True).next()
    result=[]
    dirs=sorted(dirs, key=lambda s: s.lower())
    for directoryname in dirs:
        if directoryname.startswith("."):
            continue
        element=_getFileDetails(baseDirectory, directoryname)
        element['path']=os.path.join(queriedPath, directoryname)
        element['is_dir']=True
        element['url']='%s/%s/%s' % (storeURL, queriedPath, element['name'])
        result.append(element)
    
    files=sorted(files, key=lambda s: s.lower())
    for filename in files:
        if filename.startswith("."):
            continue
        if filename in app.config['SKIPPED_FILES']:
            continue
        element=_getFileDetails(baseDirectory, filename)
        element['path']=os.path.join(queriedPath, filename)
        element['is_dir']=False
        element['url']='%s/%s/%s' % (storeURL, queriedPath, element['name'])
        result.append(element)
    nextPage=None
    return (result, nextPage)


def _getFileDetails(baseDirectory, name):
    elementpath = os.path.join(baseDirectory, name)
    element={}
    element['mtime']=os.path.getmtime(elementpath)
    element['size']=os.path.getsize(elementpath)
    element['name']=name
    return element

def getDirectoryDescription(folderPath, directory=None):
    if not directory:
        directory=folderPath
    descriptionFileName = os.path.join(directory, app.config['FOLDER_DESCRIPTION'])
    
    if not os.path.isfile(descriptionFileName):
        if os.path.samefile(folderPath, directory):
            return False
        else:
            return getDirectoryDescription(folderPath, os.path.abspath(directory+ "/.."))

    markdown_file = codecs.open(descriptionFileName, mode="r", encoding="utf-8")
    text = markdown_file.read()
    markdown_file.close()
    return markdown.markdown(text)
    #return bleach.clean(markdown.markdown(text))

def search(store, resource, text, currentPage):
    # max char search
    """
    Recoll has indeed a maximum term length of 40 characters for indexed
    terms.
    """
    pageSize=30
    db=recoll.connect(confdir=resource.getRecollDir())
    db.setAbstractParams(maxchars=80, contextwords=4)
    query = db.query()
    query.execute(text)
    query.arraysize=pageSize+1  # if we get an extra doc, then we'll return a 'nextPage' value
    query.rownumber=currentPage
    docs = query.fetchmany()
    if len(docs) > pageSize: # there are more docs beyond this page
        nextPage=currentPage+pageSize
        docs.pop()   # remove that extra doc from the results
    else:
        nextPage=None
    
    result=[]
    storePath=store.getFilesystemPath()
    storeURL=store.getStorageURL()
    for doc in docs:
        #print doc.keys()
        element={}
        element['is_dir']=False
        if doc.mtype=='inode/directory':
            element['is_dir']=True
        element['resourceName']=resource.name
        element['path']=os.path.relpath(doc.rcludi[:-1], os.path.join(storePath, resource.name))
        element['name']=doc.filename
        
        try:
            if app.config['CUSTOMERS'][store.storeID]['DONT_INCLUDE_FOLDERNAME_IN_STORAGE_URL'] == 1:
                path = element['path']
        except:
            path = 0
        if not path:
            path = os.path.relpath(doc.rcludi[:-1], storePath)

        element['url']='%s/%s' % (storeURL, path)
        element['size']=doc.fbytes
        element['mtime']=doc.mtime
        result.append(element)
    db.close()
    return (result, nextPage)


#uppath = lambda _path, n: os.sep.join(_path.split(os.sep)[:-n])

# __file__ = "/aParent/templates/blog1/page.html"
#>>> uppath(__file__, 1)
#'/aParent/templates/blog1'
#>>> uppath(__file__, 2)
#'/aParent/templates'
#>>> uppath(__file__, 3)
#'/aParent'
