#!/usr/bin/env bash

INDEX_DIR=/var/recoll

for store in $INDEX_DIR/*; do
    if [ -d ${store} ]; then
        for folder in $store/*; do
            if [ -d "${folder}" ]; then
                /usr/bin/recollindex -c "$folder"
            fi
        done
    fi
done
